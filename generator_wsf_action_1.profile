<?php

// vim: filetype=php

/************************************************************
*                           MODULES                         *
************************************************************/
function generator_wsf_action_1_profile_modules() {
    return array (
        0 => 'locale',
        1 => 'block',
        2 => 'date_api',
        3 => 'comment',
        4 => 'content',
        5 => 'content_copy',
        6 => 'date',
        7 => 'views',
        8 => 'date_copy',
        9 => 'diff',
        10 => 'email',
        11 => 'faq',
        12 => 'fieldgroup',
        13 => 'filter',
        14 => 'votingapi',
        15 => 'flag_content',
        16 => 'form_store',
        17 => 'gmap',
        18 => 'location',
        19 => 'location_views',
        20 => 'help',
        21 => 'i18n',
        22 => 'i18nblocks',
        23 => 'i18nstrings',
        24 => 'i18nmenu',
        25 => 'i18ncontent',
        26 => 'translation',
        27 => 'i18ntaxonomy',
        28 => 'i18nviews',
        29 => 'image',
        30 => 'install_profile_wizard',
        31 => 'token',
        32 => 'autolocale',
        33 => 'gmap_location',
        34 => 'location_fax',
        35 => 'location_phone',
        36 => 'gmap_views',
        37 => 'logintoboggan',
        38 => 'menu',
        39 => 'mycap',
        40 => 'taxonomy',
        41 => 'node',
        42 => 'nodefamily',
        43 => 'subform_element',
        44 => 'nodereference',
        45 => 'views_rss',
        46 => 'path',
        47 => 'ping',
        48 => 'profile_generator',
        49 => 'registration_role',
        50 => 'search',
        51 => 'nodeprofile',
        52 => 'subscriptions',
        53 => 'system',
        54 => 'nat',
        55 => 'text',
        56 => 'themesettingsapi',
        57 => 'invite',
        58 => 'i18nsync',
        59 => 'ttext',
        60 => 'update_status',
        61 => 'upload',
        62 => 'user',
        63 => 'usernode',
        64 => 'userreference',
        65 => 'calendar',
        66 => 'og',
        67 => 'views_theme_wizard',
        68 => 'views_ui',
        69 => 'fivestar',
        70 => 'watchdog',
      );
}

/************************************************************
*                           DETAILS                         *
************************************************************/
function generator_wsf_action_1_profile_details() {
    return array (
        'name' => 'Generated',
        'description' => '<p>Use <strong>Social Forum Call to Action</strong> profile insteadd.  This is a secondary one for testing and comparison purposes.</p>
        <p>Installation profile for World Social Forum Global Day of Action 2008 generated automatically on 1st Oct 2007 03:08pm by profile generator.</p>
      <p>Take 1.</p>',
      );
}

function generator_wsf_action_1_profile_final() {
/************************************************************
*                          VARIABLES                        *
************************************************************/
    variable_set('anonymous', 'Anonymous');
    variable_set('clean_url', '1');
    variable_set('comment_page', 0);
    variable_set('date_version', '-1');
    variable_set('file_directory_path', 'sites/wsf2008.net/files');
    variable_set('file_directory_temp', '/tmp');
    variable_set('file_downloads', '1');
    variable_set('filter_html_1', 1);
    variable_set('i18n_languages', array (
      'site_default' => 'en',
      'name' => 
      array (
        'zh-hans' => 'Chinese, Simplified',
        'nl' => 'Dutch',
        'fr' => 'French',
        'de' => 'German',
        'it' => 'Italian',
        'ja' => 'Japanese',
        'fa' => 'Persian',
        'pt-br' => 'Portuguese, Brazil',
        'th' => 'Thai',
        'en' => 'English',
      ),
      'rtl' => 
      array (
        0 => 1,
        'en' => 0,
        'zh-hans' => 0,
        'nl' => 0,
        'fr' => 0,
        'de' => 0,
        'it' => 0,
        'ja' => 0,
        'fa' => 0,
        'pt-br' => 0,
        'th' => 0,
      ),
      'native' => 
      array (
        'en' => 'English',
        'zh-hans' => 'Chinese, Simplified',
        'nl' => 'Dutch',
        'fr' => 'French',
        'de' => 'German',
        'it' => 'Italian',
        'ja' => 'Japanese',
        'fa' => 'Persian',
        'pt-br' => 'Portuguese, Brazil',
        'th' => 'Thai',
      ),
      'enabled' => 
      array (
        'en' => 'en',
        'zh-hans' => 'zh-hans',
        'nl' => 'nl',
        'fr' => 'fr',
        'de' => 'de',
        'it' => 'it',
        'ja' => 'ja',
        'fa' => 'fa',
        'pt-br' => 'pt-br',
        'th' => 'th',
      ),
    ));
    variable_set('menu_primary_menu', 2);
    variable_set('menu_secondary_menu', 2);
    variable_set('nodefamily_max_usernode', 1);
    variable_set('node_cron_comments_scale', 1);
    variable_set('node_cron_last', '1191216112');
    variable_set('node_cron_last_nid', '1');
    variable_set('node_cron_views_scale', 1);
    variable_set('node_options_forum', array (
      0 => 'status',
    ));
    variable_set('node_options_page', array (
      0 => 'status',
    ));
    variable_set('node_options_usernode', array (
      0 => 'status',
    ));
    variable_set('site_footer', '');
    variable_set('site_frontpage', 'node');
    variable_set('site_mail', 'support@agaricdesign.com');
    variable_set('site_mission', 'All text such as mission and slogan and title should go in themes with a t() function so they can be translated.');
    variable_set('site_name', 'WSF2008');
    variable_set('site_slogan', 'Global call for local action');
    variable_set('theme_bealestreet_wsf_settings', array (
      'toggle_logo' => 1,
      'toggle_name' => 1,
      'toggle_slogan' => 1,
      'toggle_mission' => 1,
      'toggle_node_user_picture' => 0,
      'toggle_comment_user_picture' => 0,
      'toggle_search' => 1,
      'toggle_favicon' => 1,
      'default_logo' => 1,
      'logo_path' => '',
      'logo_upload' => '',
      'default_favicon' => 1,
      'favicon_path' => '',
      'favicon_upload' => '',
      'op' => 'Save configuration',
      'form_token' => 'dd91c27672827c15e6bebd139ca9096c',
      'bealestreet_wsf_style' => 'orange',
      'bealestreet_wsf_themelogo' => 1,
      'bealestreet_wsf_width' => 0,
      'bealestreet_wsf_fixedwidth' => '850',
      'bealestreet_wsf_fontfamily' => 'Tahoma, Verdana, Arial, Helvetica, sans-serif',
      'bealestreet_wsf_customfont' => '',
      'bealestreet_wsf_uselocalcontent' => 0,
      'bealestreet_wsf_localcontentfile' => '',
      'bealestreet_wsf_breadcrumb' => 1,
      'bealestreet_wsf_iepngfix' => 0,
      'bealestreet_wsf_leftsidebarwidth' => '210',
      'bealestreet_wsf_rightsidebarwidth' => '210',
      'bealestreet_wsf_suckerfish' => 1,
    ));
    system_theme_data();
    db_query("UPDATE {system} SET status = 1 WHERE type = 'theme' and name = '%s'", 'bealestreet_wsf');
    variable_set('theme_default', 'bealestreet_wsf');
    variable_set('theme_settings', array (
      'toggle_node_info_page' => false,
    ));
    variable_set('update_status_last', 1191246760);

/************************************************************
*                         NODE TYPES                        *
************************************************************/
    db_query("INSERT INTO {node_type} (type, name, module, description, help, has_title, title_label, has_body, body_label, min_word_count, custom, modified, locked, orig_type)
               VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')",
        'page','Page','node','If you want to add a static page, like a contact page or an about page, use a page.','','1','Title','1','Body','0','1','1','0','page'
    );    db_query("INSERT INTO {node_type} (type, name, module, description, help, has_title, title_label, has_body, body_label, min_word_count, custom, modified, locked, orig_type)
               VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')",
        'page','Page','node','If you want to add a static page, like a contact page or an about page, use a page.','','1','Title','1','Body','0','1','1','0','page','story','Story','node','Stories are articles in their simplest form: they have a title, a teaser and a body, but can be extended by other modules. The teaser is part of the body too. Stories may be used as a personal blog or for news articles.','','1','Title','1','Body','0','1','1','0','story'
    );
/************************************************************
*                            ROLES                          *
************************************************************/
    $role_id['anonymous user'] = 1;
    $role_id['authenticated user'] = 2;

/************************************************************
*                            USERS                          *
************************************************************/
    $user = user_save(new stdClass(), array (
        'name' => 'submin',
        'mail' => 'support@agaricdesign.com',
        'mode' => '0',
        'sort' => '0',
        'threshold' => '0',
        'theme' => '',
        'signature' => 'Agaric Design Collective
      Open Source Free Software Web Development and DJing
      http://AgaricDesign.com/
      
      ',
        'created' => '1191204378',
        'access' => '1191269325',
        'login' => '0',
        'status' => '1',
        'timezone' => '-14400',
        'language' => '',
        'picture' => '',
        'init' => 'support@agaricdesign.com',
        'data' => 'a:1:{s:23:"subscriptions_subscribe";s:0:"";}',
      ));
    db_query("UPDATE {users} SET pass='%s' WHERE uid=%d", '68c1e36cb61d163ab2ba8cee3d7064d1', $user->uid);
    $user_id['submin'] = $user->uid;

/************************************************************
*                   USERS <=> ROLES MAPPING                 *
************************************************************/

/************************************************************
*                            MENUS                          *
************************************************************/

    while ( db_next_id('{menu}_mid') < 2) {}

    // first the primary links
    generator_wsf_action_1_profile_install_menu(2, array (
    ));
    generator_wsf_action_1_profile_install_menu(0, array (
    ));

/************************************************************
*                         URL ALIASES                       *
************************************************************/

/************************************************************
*                           BLOCKS                          *
************************************************************/

    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'translation', '0', 'garland', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'comment', '0', 'garland', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'logintoboggan', '0', 'garland', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'og', '0', 'garland', '1', '-2', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'og', '2', 'garland', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'og', '3', 'garland', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'og', '5', 'garland', '1', '-1', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'og', '6', 'garland', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'menu', $menu['2'], 'garland', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'node', '0', 'garland', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'user', '0', 'garland', '1', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'user', '1', 'garland', '1', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'user', '2', 'garland', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'user', '3', 'garland', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'views', 'comments_recent', 'garland', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'translation', '0', 'bealestreet011_wsf', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'comment', '0', 'bealestreet011_wsf', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'logintoboggan', '0', 'bealestreet011_wsf', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'og', '0', 'bealestreet011_wsf', '1', '-2', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'og', '2', 'bealestreet011_wsf', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'og', '3', 'bealestreet011_wsf', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'og', '5', 'bealestreet011_wsf', '1', '-1', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'og', '6', 'bealestreet011_wsf', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'menu', $menu['2'], 'bealestreet011_wsf', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'node', '0', 'bealestreet011_wsf', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'user', '0', 'bealestreet011_wsf', '1', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'user', '1', 'bealestreet011_wsf', '1', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'user', '2', 'bealestreet011_wsf', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'user', '3', 'bealestreet011_wsf', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'views', 'comments_recent', 'bealestreet011_wsf', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'translation', '0', 'bealestreet_wsf', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'comment', '0', 'bealestreet_wsf', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'logintoboggan', '0', 'bealestreet_wsf', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'og', '0', 'bealestreet_wsf', '1', '-2', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'og', '2', 'bealestreet_wsf', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'og', '3', 'bealestreet_wsf', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'og', '5', 'bealestreet_wsf', '1', '-1', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'og', '6', 'bealestreet_wsf', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'menu', $menu['2'], 'bealestreet_wsf', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'node', '0', 'bealestreet_wsf', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'user', '0', 'bealestreet_wsf', '1', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'user', '1', 'bealestreet_wsf', '1', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'user', '2', 'bealestreet_wsf', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'user', '3', 'bealestreet_wsf', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'views', 'comments_recent', 'bealestreet_wsf', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'translation', '0', 'bluemarine', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'comment', '0', 'bluemarine', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'logintoboggan', '0', 'bluemarine', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'og', '0', 'bluemarine', '1', '-2', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'og', '2', 'bluemarine', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'og', '3', 'bluemarine', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'og', '5', 'bluemarine', '1', '-1', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'og', '6', 'bluemarine', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'menu', $menu['2'], 'bluemarine', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'node', '0', 'bluemarine', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'user', '0', 'bluemarine', '1', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'user', '1', 'bluemarine', '1', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'user', '2', 'bluemarine', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'user', '3', 'bluemarine', '0', '0', 'left', '0', '0', '0', '', ''
    );
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'views', 'comments_recent', 'bluemarine', '0', '0', 'left', '0', '0', '0', '', ''
    );

/************************************************************
*                       EXPORTING NODES                     *
************************************************************/

    system_initialize_theme_blocks('bealestreet_wsf');

    return;
}

function generator_wsf_action_1_profile_install_menu($pid, $menu) {
    foreach ( $menu as $item) {
        $mid = db_next_id('{menu}_mid');
        db_query("INSERT INTO {menu} (mid, pid, path, title, description, weight, type) VALUES (%d,%d,'%s','%s','%s', %d, %d)",
                 $mid, $pid, $item['path'], $item['title'], $item['description'], $item['weight'], $item['type']);
        generator_wsf_action_1_profile_install_menu($mid, $item['children']);
    }
}

?>
